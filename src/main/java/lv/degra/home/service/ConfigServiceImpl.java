package lv.degra.home.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lv.degra.home.model.Config;
import lv.degra.home.model.ConfigRepo;

@Service
@AllArgsConstructor
public class ConfigServiceImpl implements ConfigService {

	@Autowired
	private ConfigRepo configRepo;
	public Config saveConfig(Config config) {
		return configRepo.save(config);
	}
	@Override
	public Config readConfig() {
		return configRepo.findFirstByOrderByIdDesc().orElse(null);
	}
}
