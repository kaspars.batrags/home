package lv.degra.home.controller;

import static lv.degra.home.HomeApplication.REST_API_URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lv.degra.home.model.Config;
import lv.degra.home.service.ConfigService;

@RestController
@RequestMapping(REST_API_URL + ConfigController.ENDPOINT_NAME)
@AllArgsConstructor
public class ConfigController {
	static final String ENDPOINT_NAME = "/config";
	@Autowired
	private ConfigService configService;

	@GetMapping()
	@ResponseStatus(value = HttpStatus.OK)
	public Config getConfig() {
		return configService.readConfig();
	}

	@PutMapping
	@ResponseStatus(value = HttpStatus.OK)
	public Config saveConfig(@RequestBody Config config) {
		return configService.saveConfig(config);
	}

	@GetMapping(path = "/health")
	public String getHealthStatus() {
		return "OK";
	}
}
