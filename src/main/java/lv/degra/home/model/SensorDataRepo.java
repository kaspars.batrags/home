package lv.degra.home.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SensorDataRepo extends JpaRepository<SensorData, Integer> {

	@Query("SELECT e FROM SensorData e ORDER BY e.id DESC LIMIT 10")
	List<SensorData> findLast10Records();

}
