package lv.degra.home.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import lv.degra.home.model.Config;
import lv.degra.home.model.ConfigRepo;

class ConfigServiceImplTest {

	@InjectMocks
	private ConfigServiceImpl configService;

	@Mock
	private ConfigRepo configRepo;

	@BeforeEach
	public void init() {
		initMocks(this);
	}

	@Test
	void testSaveConfig() {
		Config configToSave = new Config();
		// Set up any necessary data for the test

		// Mock the behavior of configRepo.save
		when(configRepo.save(configToSave)).thenReturn(configToSave);

		Config savedConfig = configService.saveConfig(configToSave);

		// Verify that the save method was called and the returned object matches our expectations
		assertEquals(configToSave, savedConfig);
	}

	@Test
	void testReadConfig() {
		Config expectedConfig = new Config();
		// Set up any necessary data for the test

		// Mock the behavior of configRepo.findFirstByOrderByIdDesc
		when(configRepo.findFirstByOrderByIdDesc()).thenReturn(Optional.of(expectedConfig));

		Config readConfig = configService.readConfig();

		// Verify that the find method was called and the returned object matches our expectations
		assertEquals(expectedConfig, readConfig);
	}
}
