package lv.degra.home.controller;

import static lv.degra.home.HomeApplication.REST_API_URL;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lv.degra.home.model.SensorData;
import lv.degra.home.service.SensorDataService;

@RestController
@RequestMapping(REST_API_URL + SensorDataController.ENDPOINT_NAME)
@AllArgsConstructor

public class SensorDataController {
	static final String ENDPOINT_NAME = "/sensor_data";
	@Autowired
	private SensorDataService sensorDataService;

	@GetMapping()
	@ResponseStatus(value = HttpStatus.OK)
	public List<SensorData> getLast10SensorData() {
		return sensorDataService.getLast10SensorData();
	}

	@PutMapping
	@ResponseStatus(value = HttpStatus.OK)
	public SensorData saveSensorData(@RequestBody SensorData sensorData) {
		return sensorDataService.saveSensorData(sensorData);
	}

	@GetMapping(path = "/health")
	public String getHealthStatus() {
		return "OK";
	}
}
