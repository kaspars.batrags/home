package lv.degra.home.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lv.degra.home.model.SensorData;
import lv.degra.home.model.SensorDataRepo;

@Service
@AllArgsConstructor
public class SensorDataServiceImpl implements SensorDataService {

	@Autowired
	private SensorDataRepo sensorDataRepo;

	public SensorData saveSensorData(SensorData sensorData) {
		return sensorDataRepo.save(sensorData);
	}

	public List<SensorData> getLast10SensorData() {
		return sensorDataRepo.findLast10Records();
	}

}
