package lv.degra.home.model;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ConfigRepo extends JpaRepository<Config, Integer> {
	Optional<Config> findFirstByOrderByIdDesc();
}
