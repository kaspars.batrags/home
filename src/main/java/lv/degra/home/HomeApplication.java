package lv.degra.home;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeApplication {

	public final static String REST_API_URL = "/API";
	public static void main(String[] args) {
		SpringApplication.run(HomeApplication.class, args);
	}

}
