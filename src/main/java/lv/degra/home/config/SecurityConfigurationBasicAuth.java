package lv.degra.home.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AuthorizeHttpRequestsConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import jakarta.servlet.DispatcherType;

@Configuration
public class SecurityConfigurationBasicAuth {

	private static final String SERVE_ROLE_NAME = "SERVER";
	@Value("${api_username}")
	private String useName;
	@Value("${api_password}")
	private String userPassword;

	private static void customize(
			AuthorizeHttpRequestsConfigurer<HttpSecurity>.AuthorizationManagerRequestMatcherRegistry authorizeRequests) {
		((AuthorizeHttpRequestsConfigurer.AuthorizedUrl) ((AuthorizeHttpRequestsConfigurer.AuthorizedUrl) ((AuthorizeHttpRequestsConfigurer.AuthorizedUrl) ((AuthorizeHttpRequestsConfigurer.AuthorizedUrl) ((AuthorizeHttpRequestsConfigurer.AuthorizedUrl) authorizeRequests.dispatcherTypeMatchers(
				HttpMethod.PUT, new DispatcherType[] { DispatcherType.INCLUDE })).permitAll()
				.requestMatchers(new String[] { "/API/sensor_data/**" })).hasRole(SERVE_ROLE_NAME)
				.dispatcherTypeMatchers(HttpMethod.GET, new DispatcherType[] { DispatcherType.INCLUDE })).permitAll()
				.requestMatchers(new String[] { "/API/**" })).hasRole("SERVER").anyRequest()).authenticated();
	}

	@Bean
	public PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public InMemoryUserDetailsManager userDetailsService() {
		UserDetails user1 = User.builder()
				.username(useName)
				.password(encoder().encode(userPassword))
				.roles(SERVE_ROLE_NAME)
				.build();
		return new InMemoryUserDetailsManager(user1);
	}

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeHttpRequests(SecurityConfigurationBasicAuth::customize).httpBasic(Customizer.withDefaults());
		return http.build();
	}




}