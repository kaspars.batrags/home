package lv.degra.home.service;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.List;
import java.util.ArrayList;

import lv.degra.home.model.SensorData;
import lv.degra.home.model.SensorDataRepo;

public class SensorDataServiceImplTest {
	private SensorDataServiceImpl sensorDataService;
	private SensorDataRepo sensorDataRepo;

	@BeforeEach
	public void setUp() {
		sensorDataRepo = mock(SensorDataRepo.class);
		sensorDataService = new SensorDataServiceImpl(sensorDataRepo);
	}

	@Test
	public void testSaveSensorData() {
		SensorData sensorDataToSave = new SensorData(/* initialize sensor data as needed */);
		when(sensorDataRepo.save(sensorDataToSave)).thenReturn(sensorDataToSave);

		SensorData savedSensorData = sensorDataService.saveSensorData(sensorDataToSave);

		verify(sensorDataRepo, times(1)).save(sensorDataToSave);
		assertEquals(sensorDataToSave, savedSensorData);
	}

	@Test
	public void testGetLast10SensorData() {
		List<SensorData> sensorDataList = new ArrayList<>();
		// Add test data to the list as needed

		when(sensorDataRepo.findLast10Records()).thenReturn(sensorDataList);

		List<SensorData> last10SensorData = sensorDataService.getLast10SensorData();

		verify(sensorDataRepo, times(1)).findLast10Records();
		assertEquals(sensorDataList, last10SensorData);
	}
}
