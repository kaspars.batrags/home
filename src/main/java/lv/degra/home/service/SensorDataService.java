package lv.degra.home.service;

import java.util.List;

import lv.degra.home.model.SensorData;

public interface SensorDataService {

	SensorData saveSensorData(SensorData sensorData);
	List<SensorData> getLast10SensorData();
}
