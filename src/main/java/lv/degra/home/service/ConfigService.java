package lv.degra.home.service;

import lv.degra.home.model.Config;

public interface ConfigService {
	Config saveConfig(Config config);

	Config readConfig();
}
